{
  stdenv,
  cmake,
  lib,
}:
stdenv.mkDerivation {
  name = "cpp-playground";

  src = lib.cleanSource ./.;

  nativeBuildInputs = [
    cmake
  ];
}
