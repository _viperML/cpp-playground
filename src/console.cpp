#include <iostream>
#include <memory>
#include <vector>

class Circle {
public:
  int color;
  void paint();
  Circle();
};

Circle::Circle() { color = 0; }

void Circle::paint() { color = 1; }

std::vector<int> my_vec = {
  0,
  1,
  2,
};

int main() {
  std::cout << "Hello World" << '\n';
  auto name = std::string("Fernando");
  std::cout << "Hello " << name << '\n';

  auto c = Circle();
  c.paint();

  std::cout << c.color << '\n';

  auto c_ptr = std::make_shared<Circle>();

  c_ptr->paint();

  std::cout << c_ptr->color << '\n';

  for (auto elem : my_vec) {
    std::cout << "Vec elem: " << elem << '\n';
  }
}
